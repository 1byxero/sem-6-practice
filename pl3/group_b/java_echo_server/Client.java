import java.io.*;
import java.net.*;


class Client{

	public static void main(String[] args){

		Socket client = null;
		BufferedReader is = null;
		DataOutputStream os = null;

		try{
			BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Input host ip");
			String hostname = keyboard.readLine();
			System.out.println("Input host port");
			int port = Integer.parseInt(keyboard.readLine());
			client = new Socket(hostname,port);

			os = new DataOutputStream(client.getOutputStream());

			is = new BufferedReader(new InputStreamReader(client.getInputStream()));

		} catch(Exception e){
			System.err.println(e);
		}


		try{

				while(true){
					System.out.println("Enter input to send to echo (0 to stop connection, -1 to stop server)");
					BufferedReader keyboard2 = new BufferedReader(new InputStreamReader(System.in));
					String input = keyboard2.readLine();
					os.writeBytes(input+"\n");
					if(input.equals("0") || input.equals("-1")){
						break;
					}
					String echoresponse = is.readLine();
					System.out.println("Echo server respnoded with '"+echoresponse+"'");
				}

		//close sockets

		client.close();
		os.close();
		is.close();
		

		}catch(Exception e){
			System.err.println(e);
		}
	}
}