import java.io.*;
import java.net.*;


public class Server{

	/*
	declare server and client socket
	declare necesarry variables
	create methods to start,stop server and accept connections
	create method to handle threads
	create a bufferedreader to read from connections
	create printstream to 
	*/

	ServerSocket server = null;
	Socket client = null;
	int connection_no = 0;
	int port;


	public Server(int port)	{
		this.port = port;		
	}

	void stopserver(){	
			System.out.println("Shutting echo server...");
			System.exit(0);			
	}

	void startserver(){

		try{
			server = new ServerSocket(port);

			System.out.println("Socket created and listening at port "+port+" ");


			while(true){
				try{
					client = server.accept();
					connection_no++;
					//create a thread for handling the communication
					Connectionhandler connection = new Connectionhandler(client,connection_no,this);
					new Thread(connection).start();

				} catch(Exception e){
					System.out.println("Following exception occured while accepting a client connection\n\n");
					System.out.println(e);
				}				
			}

		} catch(Exception e){
			System.out.println("Following exception occured while creating socket\n\n");
			System.out.println(e);


		}

	}



	public static void main(String args[]){
		try{
			System.out.println("Enter port to host the echo server");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
			String port = br.readLine();				
			br.close();
			Server server = new Server(Integer.parseInt(port));
			server.startserver();
		} catch(Exception e){
				System.out.println(e);

		}

	}
}

class Connectionhandler implements Runnable{
	BufferedReader is;
	PrintStream os;
	int id;
	Socket client;
	Server server;

	public Connectionhandler(Socket client,int connection_no,Server server){
		this.client = client;
		this.id = connection_no;
		this.server = server;
		System.out.println("Connection "+id+" established with "+client);

		try{
			is = new BufferedReader(new InputStreamReader(client.getInputStream()));
			os = new PrintStream(client.getOutputStream());	
		} catch(Exception e) {
			System.out.println(e);
		}
	}
		public void run(){			
			String line;
			try{
				boolean stopserverbool = false;
				while (true){
					line = is.readLine();
					System.out.println("Received '"+line+"' from connection "+id);
					if(line.equals("-1")){
						System.out.println("Shutting down the server");
						stopserverbool = true;
						break;
					}
					if(line.equals("0")){
						break;
						}
					os.println(line);
					}

				System.out.println("Closing connection with id "+id);
				is.close();
				os.close();
				client.close();

				if(stopserverbool==true){
					server.stopserver();
				}
				} catch (Exception e){
				System.out.println(e);
			}
		}
	}
	
