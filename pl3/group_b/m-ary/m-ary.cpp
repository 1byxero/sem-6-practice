#include <iostream>
#include <omp.h>
using namespace std;


/*
input phase
separator value setting phase;
search phase
*/

int main(){
		//input phase
		int left,right,size,partitioning_n,interval,key,i,tid;
		
		cout<<"Enter size of the array\n";
		cin>>size;
		int array[size],separator[size];
		cout<<"Enter elements of array in ascending order\n";
		for (int i=0;i<size;i++){
			cin>>array[i];
		}
		cout<<"Enter value or 'n' for n-ary search\n";
		cin>>partitioning_n;
		cout<<"Enter key to search\n";
		cin>>key;

		left = 0;
		right = size -1;
		bool element_found = false;
		//input phase ends

		

		if(key>=array[left] && key<=array[right]){

			while(left!=right){

				//separator value setting phase
				cout<<"left = "<<left<<" right = "<<right<<" size = "<<size<<endl;
				if(partitioning_n>=size){
					#pragma omp parallel for
					for(i=0;i<size;i++){
						separator[i] = left+i;
					}
					if(partitioning_n==size){
						separator[size] = left+i;	
					}					
				}else{

					separator[0]=left;
					interval = (size/partitioning_n)+1; ///u made mistake here
					#pragma omp parallel for					
					for(i=1;i<=partitioning_n-1;i++){
						separator[i] = left+(interval*i) - 1;
						tid = omp_get_thread_num();						
						cout<<"Thread with id "<<tid<<" allocated to separator["<<i<<"]="<<separator[i]<<endl;
					}
					separator[partitioning_n] = right;					
				}
				//separator value setting phase

				//search phase

				for(i=0;i<=partitioning_n;i++){
					if(key==array[separator[i]]){												
						cout<<"Element found at index "<<separator[i]+1<<endl;
						element_found = true;
						break;
					}
					if(key<array[separator[i]]){		//made a mistake in this loops
						right= separator[i];
						left = separator[i-1]+1;
						size = right-left+1;
						break;
					}
				}
				if(element_found)
					break;
				//search phase		
			}
		}
		else if(!(key>array[left] && key<array[right]) || left==right) {
			cout<<"Element not found in given input array\n";
		}
	return 0;	
}

