#include <iostream>
#include <cuda.h>
#define N 5
using namespace std;

#define swap(A,B){int temp = A; A = B; B=temp;}

__global__ void sort(int *c, int *count){
	int l;
	if((*count%2)==0){
		l = *count/2;
	}
	else{
		l = *count/2+1;	
	}

	for (int i=0;i<l;i++){
		//even part
		if((!threadIdx.x&1) && threadIdx.x<(*count-1)){
				if(c[threadIdx.x]>c[threadIdx.x+1]){
					swap(c[threadIdx.x],c[threadIdx.x+1]);
				}
			}
		//odd part
		if((threadIdx.x&1) && threadIdx.x<(*count-1)){
				if(c[threadIdx.x]>c[threadIdx.x+1]){
					swap(c[threadIdx.x],c[threadIdx.x+1]);
				}
			}
			__syncthreads();
		}
	}


int main(){
	int a[N],b[N],n;

	cout<<"Enter size of the array\n";
	cin>>n;
	if(n>N){
		cout<<"size too large";
		return 1;
	}
	cout<<"Enter elements of array\n";
	for (int i=0;i<n;i++){
		cin>>a[i];
	}
	cout<<"Elements of array are\n";
	for (int i=0;i<n;i++){
		cout<<a[i]<<endl;
	}

	int *c,*count;
	cudaMalloc((void**) &c,sizeof(int)*N);
	cudaMalloc((void**) &count,sizeof(int));
	cudaMemcpy(c,&a,sizeof(int)*N,cudaMemcpyHostToDevice);
	cudaMemcpy(count,&n,sizeof(int),cudaMemcpyHostToDevice);
	sort<<<1,n>>>(c,count);
	cudaMemcpy(&b,c,sizeof(int)*N,cudaMemcpyDeviceToHost);
	cout<<"Sorted elements of array are\n";
	for(int i=0;i<n;i++){
		cout<<b[i]<<endl;
	}


		

	return 0;
}