#include<stdio.h>
#include<CL/cl.h>
#define data_size 10

const char *codesource = "__kernel void vectoradd(__global float *InputA, __global float *InputB, __global float *output)"
	"{\n"
	"size_t id = get_global_id(0);\n"
	"output[id] = InputA[id]+InputA[id];\n"
	"};";


int main(){

	int InputDataA[data_size] = {1,2,3,4,5,6,7,8,9,10}; 
	int InputDataB[data_size] = {1,2,3,4,5,6,7,8,9,10};
	int result[data_size];

	cl_platform_id platform_id;
	cl_uint num_platforms;
	cl_device_id device_id;
	cl_uint num_devices;
	cl_context_properties properties[3];
	cl_context context;
	cl_int error;
	cl_command_queue command_queue;
	cl_program program;
	cl_kernel kernel;
	cl_mem InputA,InputB,output;

	if((clGetPlatformIDs(1, &platform_id, &num_platforms))!=CL_SUCCESS){
	printf("Error 	clGetPlatformID");
	return 1;
	}
	
	if((clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 1,&device_id, &num_devices))!=CL_SUCCESS){
	printf("Error 	clGetDEviceID");
	return 1;
	}



	properties[0] = (CL_CONTEXT_PLATFORM);
	properties[1] = (cl_context_properties) platform_id;
	properties[2] = 0;
	
	if(clCreateContext(properties, num_devices, &device_id,NULL, NULL, &error)!=CL_SUCCESS)
	{
	printf("Error 	context");
	return 1;	
	}
	
	command_queue = clCreateCommandQueue(context, device_id,0,&error);
	
	program = clCreateProgramWithSource(context, 1, (const char **) &codesource,NULL,&error);
	
	if(clBuildProgram(program , 0, NULL, NULL, NULL,NULL)!=CL_SUCCESS){
	printf("Error 	BUILDprogram");
return 1;	}

	kernel = clCreateKernel(program, "vectoradd", &error);

	InputA = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(float)*data_size,NULL,NULL);
	InputB = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(float)*data_size,NULL,NULL);
	output = clCreateBuffer(context, CL_MEM_WRITE_ONLY,sizeof(float)*data_size,NULL,NULL);

	clEnqueueWriteBuffer(command_queue,InputA,CL_TRUE, 0, sizeof(float)*data_size, InputDataA,NULL,NULL,NULL);
	clEnqueueWriteBuffer(command_queue,InputB,CL_TRUE, 0, sizeof(float)*data_size, InputDataB,NULL,NULL,NULL);
	
	clSetKernelArg(kernel,0,sizeof(cl_mem),&InputA);
	clSetKernelArg(kernel,1,sizeof(cl_mem),&InputB);
	clSetKernelArg(kernel,3,sizeof(cl_mem),&output);

	size_t global = data_size;
	
	clEnqueueNDRangeKernel(command_queue,kernel,1,NULL,&global,NULL,0,NULL,NULL);
	clFinish(command_queue);

	clEnqueueReadBuffer(command_queue,output,CL_TRUE,0,sizeof(float)*data_size,result,NULL,NULL,NULL);
	
	printf("The output is\n");

	for(int i=0;i<data_size;i++){
		printf("%f",result[i]);
	}
	
	clReleaseMemObject(InputA);
	clReleaseMemObject(InputB);
	clReleaseMemObject(output);
	clReleaseContext(context);
	clReleaseCommandQueue(command_queue);
	clReleaseKernel(kernel);
	clReleaseProgram(program);

	/*
	write code	
	define variables;
	getplatformid
	getdeviceid
	define properties
	create context
	create commandqueue
	create code
	compile code
	create kernel
	create bufferes
	enquebufferestowrite
	setkernelargs
	RUN kernel til clfinish
	read bufferes
	print op
	free clshit	
	*/
	return 0;
}


