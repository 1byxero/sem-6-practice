from Adafruit_BBIO import GPIO as g
import time


"""
steps to do
step 1: take/set input as which pins are output
step 2: initilize the output pins and set all of them to low
step 3: start the normal signal
step 4: start the normal signal
"""

def takeip():
	#step 1
	signal1red = raw_input("input the pin which outputs as red signal 1")
	signal1yellow = raw_input("input the pin which outputs as yellow signal 1")
	signal1green = raw_input("input the pin which outputs as green signal 1")
	signal2red = raw_input("input the pin which outputs as red signal 2")
	signal2yellow = raw_input("input the pin which outputs as yellow signal 2")
	signal2green = raw_input("input the pin which outputs as green signal 2")	
	return signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green

def setalloutputlow(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green):
	g.output(signal1red,g.LOW)
	g.output(signal1yellow,g.LOW)
	g.output(signal1green,g.LOW)
	g.output(signal2red,g.LOW)
	g.output(signal2yellow,g.LOW)
	g.output(signal2green,g.LOW)

def init(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green):
	#step 2
	g.setup(signal1red,g.OUT)
	g.setup(signal1yellow,g.OUT)
	g.setup(signal1green,g.OUT)
	g.setup(signal2red,g.OUT)
	g.setup(signal2yellow,g.OUT)
	g.setup(signal2green,g.OUT)
	#ALL SET TO LOW
	setalloutputlow(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)
	




def signalnormal(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green):
	#signal 3
	setalloutputlow(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)
	for i in xrange(5): #loop to simulate signal for 5 iterations
		#set signal 1 red and signal 2 green
		g.output(signal1red,g.HIGH)
		g.output(signal2green,g.HIGH)
		time.sleep(3)
		#set signal 2 yellow
		g.output(signal2green,g.LOW)
		g.output(signal2yellow,g.HIGH)
		time.sleep(1)
		#set signal 2 red and signal 1 green
		g.output(signal2yellow,g.LOW)
		g.output(signal2red,g.HIGH)
		g.output(signal1green,g.HIGH)
		time.sleep(3)
		#set signal 1 yellow
		g.output(signal1green,g.LOW)
		g.output(signal1yellow,g.HIGH)
		time.sleep(1)
		#set signal 1 red and signal 2 green
		g.output(signal1yellow,g.LOW)
		g.output(signal1red,g.HIGH)
		g.output(signal2green,g.HIGH)
		time.sleep(3)
		print str(i)" iteration completed"
	setalloutputlow(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)	

def signalnoon(signal1yellow,signal2yellow):
	g.output(signal1yellow,g.HIGH)
	g.output(signal2yellow,g.HIGH)
	time.sleep(1)
	g.output(signal1yellow,g.LOW)
	g.output(signal2yellow,g.LOW)
	time.sleep(1)


def signalnooniteration(signal1yellow,signal2yellow):
	#step 4
	setalloutputlow(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)
	for i in xrange(5): #5 iterations
		signalnoon(signal1yellow,signal2yellow)		
	setalloutputlow(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)	

def main():	
	signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green = takeip()
	print "initializing the output pins"
	init(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)
	print "initialized the output pins"
	print "simulating normal signal"
	signalnormal(signal1red,signal1yellow,signal1green,signal2red,signal2yellow,signal2green)	
	print "normal signal simulated"
	print "simulating noon signal"
	signalnooniteration(signal1yellow,signal2yellow)
	print "noon signal simulated"

if __name__ == '__name__':
	main()