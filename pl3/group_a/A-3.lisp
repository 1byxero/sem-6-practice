(defvar a)
(defvar b)
(defvar c)
(defvar temp)
(defvar lol)
(defvar i)
(defvar j)
(defvar sum)
(defvar qwerty)

(write-line "Enter 1st number:")
(setq a (read))
(write-line "Enter 2nd number:")
(setq b (read))
(setq sum 0)
(setf lol (make-array '(64)))

;Addition
(defun add()			
	(setf c(+ a b))
	(print "SUM")
	(print "(in decimal):")
	(write c)
	(print "(in binary):")
	(write(format nil "~b" c))
	(terpri)
)

;Subtraction
(defun sub()
	(setf c(- a b))
	(print "SUBTRACTION:")
	(print "(in decimal):")
	(write c)
	(print "(in binary):")
	(write(format nil "~b" c))
	(terpri)
	
)



;Division
(defun div()
	(setf c(/ a b))
	(print "DIVISION:")
	(print "(in decimal):")
	(write c)
	(print "(in binary):")
	(write(format nil "~b" c))
	(terpri)
)

(sb-thread:make-thread(lambda() (add)))

(sb-thread:make-thread(lambda() (sub)))

(sb-thread:make-thread(lambda() (div)))

(write (sb-thread:list-all-threads))


;multiply every bit with 1st number and store in lol array

;for a*b
;for i = 0 to 64
;	right shift b by i
;	longand b with 1 if not zero then find left shift of a by i and save it to lol[i]

;for i = 0 to 64
;	add lol[i] to get sum

(defun mult(i)
	(setf i(- 0 i))
	(setf qwerty (ash b i))	; shift right
	(setf qwerty (logand qwerty 1))
	
	(cond 
		((= qwerty 0) (setf temp 0))
		(t (setf temp a))

	)


	(setf i(- 0 i))
	(setf (aref lol i) (ash temp i))
)
(setq j 0)
(loop
	(sb-thread:make-thread(lambda() (mult j)))
	(setf j(+ j 1))
	(when (= j 64) (return j))
)

(setq i 0)
(loop
	(setf sum(+ sum (aref lol i)))
	(setf i(+ i 1))	
	(when (= i 64) (return i))
)

(print "Multiplication:")
(print "(in decimal):")
(write sum)
(print "(in binary):")
(write(format nil "~b" sum))
(print "(in hex):")
(write(format nil "~x" sum))
(terpri)

(exit)