%{

#include<stdio.h>
#include<omp.h>
#define YYSTYPE int

	int numthreads,expperthread;
	char nameoffile[100];

%}

%token NUMBER

%left '+' '-'
%left '*' '/' '%'
%left '(' ')'

%pure-parser
%lex-param{void *scanner}
%parse-param{void *scanner}

%%

ArithmaticExpression: E{
	return $$;
}
	E:E'+'E {$$=$1+$3;}
	|E'-'E {$$=$1-$3;}
	|E'*'E {$$=$1*$3;}
	|E'/'E {$$=$1/$3;}
	|E'%'E {$$=$1%$3;}
	|'('E')' {$$=$2;}
	| NUMBER {$$=$1;}
;

%%

int func(){

	void *scanner;
	yylex_init(&scanner);
	
	int i,j,tid,res;

	FILE *fp = fopen(nameoffile,"r");

	tid = omp_get_thread_num();

	char *s = malloc(100);
	size_t n = 100;		

	for(i=0;i<(tid*expperthread);i++){
		getline(&s,&n,fp);
	}

	yyset_in(fp,scanner);

	for(i=0;i<(expperthread);i++){

		res=yyparse(scanner);
		printf("%dth expression with answer %d is run by thread %d\n",(tid*expperthread+i+1),res,tid);
	}
	yylex_destroy(scanner);

	fclose(fp);
	return 0;
}

int main(){

	printf("Enter name of the file\n");
	scanf("%s",nameoffile);
	printf("Enter number of threads to create\n");
	scanf("%d",&numthreads);
	printf("Enter number of expressions per thread\n");
	scanf("%d",&expperthread);

	#pragma omp parallel num_threads(numthreads)
	{
	func();
	}

	return 0;	
}

int yyerror(){
	printf("Expression invalid\n");
	return 1;
}