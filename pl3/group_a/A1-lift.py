from Adafruit_BBIO import GPIO as g
import time


"""
steps to do
step 1: take input as which gpio pins are input and output pins
step 2: first initialize the gpio pins as input and output pins
step 3: set all outputs to low, create a variable to keep the current state of lift in memory
step 4: make loop to listen inputs from switch
step 5: make functions to move the lift
"""



def takeip():
	"""
	step 1
	input pins are connected to switch to indicate the requirement of life at certain floor
	ouput pins are connected to led or gate to indicate the arriaval of life at the floor
	floor door indicated the floor door is open
	"""
	
	ip1 = raw_input("Enter pin number of input pin 1\n")
	ip2 = raw_input("Enter pin number of input pin 2\n")
	ip3 = raw_input("Enter pin number of input pin 3\n")
	op1 = raw_input("Enter pin number of output pin 1\n")
	op2 = raw_input("Enter pin number of output pin 2\n")
	op3 = raw_input("Enter pin number of output pin 3\n")
	floordoor = raw_input("Enter pin number for output of floor door\n")
	return ip1,ip2,ip3,op1,op2,op3,floordoor


def init(ip1,ip2,ip3,op1,op2,op3,floordoor):
	"""
	first setup the pins as input or output
	step 2
	"""	
	g.setup(ip1,g.IN)
	g.setup(ip2,g.IN)
	g.setup(ip3,g.IN)	
	g.setup(ip1,g.OUT)
	g.setup(ip2,g.OUT)
	g.setup(ip3,g.OUT)
	g.setup(floordoor,g.OUT)	
	"""
	step3
	then make all the output pins low	
	"""
	g.output(ip1,g.LOW)
	g.output(ip2,g.LOW)
	g.output(ip3,g.LOW)
	g.output(floordoor,g.LOW)

"""
step 5: make functions to move the lift
"""
def firston(op1):
	g.output(op1,g.HIGH)

def firstoff(op1):
	g.output(op1,g.LOW)

def secondon(op2):
	g.output(op2,g.HIGH)	

def secondoff(op2):
	g.output(op2,g.LOW)

def thirdon(op3):
	g.output(op3,g.HIGH)

def thirdoff(op3):
	g.output(op3,g.LOW)

def floordooropen(floordoor):
	g.output(floordoor,g.HIGH)
	print "Door open"
	sleep(2)
	g.output(floordoor,g.LOW)
	print "Door closed"


def fromonetoone(op1,floordoor):
	firston(op1)
	sleep(2)	
	floordooropen(floordoor)
	print "Lift reached first floor"

def fromonetotwo(op1,op2,floordoor):
	firston(op1)
	sleep(2)
	firstoff(op1)
	secondon(op2)
	floordooropen(floordoor)
	print "Lift reached second floor"

def fromonetothree(op1,op2,op3,floordoor):
	firston(op1)
	sleep(2)
	firstoff(op1)
	secondon(op2)
	sleep(2)
	secondoff(op2)	
	thirdon(op3)
	floordooropen(floordoor)
	print "Lift reached third floor"

def fromtwotoone(op1,op2,floordoor):
	secondon(op1)
	sleep(2)
	secondoff(op2)
	firston(op1)
	floordooropen(floordoor)
	print "Lift reached first floor"

def fromtwototwo(op2,floordoor):
	secondon(op2)
	sleep(2)	
	floordooropen(floordoor)
	print "Lift reached second floor"
	

def fromtwotothree(op2,op3,floordoor):
	secondon(op2)
	sleep(2)
	secondoff(op2)
	thirdon(op3)
	floordooropen(floordoor)
	print "Lift reached third floor"

def fromthreetoone(op1,op2,op3,floordoor):
	thirdon(op3)
	sleep(2)
	thirdoff(op3)
	secondon(op2)
	sleep(2)
	secondoff(op2)
	firston(op1)
	floordooropen(floordoor)
	print "Lift reached first floor"

def fromthreetotwo(op2,op3,floordoor):
	thirdon(op3)
	sleep(2)
	thirdoff(op3)
	secondon(op2)
	floordooropen(floordoor)
	print "Lift reached second floor"


def fromthreetothree(op3,floordoor):
	thirdon(op3)
	sleep(2)	
	floordooropen(floordoor)
	print "Lift reached third floor"


def dothething(ip1,ip2,ip3,op1,op2,op3,floordoor):
	"""
	step 4: make loop to listen inputs from switch
	"""
	liftat = 1 #initially lift at floor 1, step 3 lift state variable

	while 1:		#listen for infinite time 
		if g.input(ip1)==1:			#listen for floor 1 call
			print "lift called on floor 1"
			if liftat == 2:
				print "sending lift from floor two to one"
				fromtwotoone(op1,op2,floordoor):		#send lift from two to one
				liftat = 1
			elif liftat ==3:
				print "sending lift from floor three to one"
				fromthreetoone(op1,op2,op3,floordoor): 		#send lift from three to one
				liftat = 1
			else:
				print "lift already at floor one"
				fromonetoone(op1,floordoor)			#send lift from one to one
		if g.input(ip2)==1:			#listen for floor 2 call
			print "lift called on floor 2"
			if liftat == 1:
				print "sending lift from floor one to two"
				fromonetotwo(op1,op2,floordoor):		#send lift from one to two
				liftat = 2
			elif liftat ==3:
				print "sending lift from floor three to two"
				fromthreetotwo(op2,op3,floordoor): 		#send lift from three to two
				liftat = 2
			else:
				print "lift already at floor two"
				fromtwototwo(op2,floordoor)			#send lift from two to two
		if g.input(ip3)==1:			#listen for floor 2 call
			print "lift called on floor 3"
			if liftat == 1:
				print "sending lift from floor one to three"
				fromonetothree(op1,op2,op3,floordoor):		#send lift from one to three
				liftat = 3
			elif liftat ==2:
				print "sending lift from floor two to three"
				fromtwotothree(op2,op3,floordoor): 		#send lift from two to three
				liftat = 3
			else:
				print "lift already at floor three"
				fromthreetothree(op3,floordoor)			#send lift from three to three

def main():	
	ip1,ip2,ip3,op1,op2,op3,floordoor = takeip()	#get info of interfaced pins
	init(ip1,ip2,ip3,op1,op2,op3,floordoor)			#initialize the pins as input,output and set output pins to low
	dothething(ip1,ip2,ip3,op1,op2,op3,floordoor)	#start listening


	

if __name__ == '__name__':
	main()